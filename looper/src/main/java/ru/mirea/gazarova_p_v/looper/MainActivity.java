package ru.mirea.gazarova_p_v.looper;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;

import ru.mirea.gazarova_p_v.looper.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    MyLooper myLooper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Handler mainThreadHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Log.d(MainActivity.class.getSimpleName(), "Task execute. This is result: " + msg.getData().getString("result"));
            }
        };
        myLooper = new MyLooper(mainThreadHandler);
        myLooper.start();
    }

    public void onClick(View v) {
        Message msg = Message.obtain();
        Bundle bundle = new Bundle();
        bundle.putString("KEY", "Студент");
        bundle.putInt("Vosrast", 23);
        msg.setData(bundle);
        myLooper.mHandler.sendMessage(msg);

    }
}