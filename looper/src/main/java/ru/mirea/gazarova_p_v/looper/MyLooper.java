package ru.mirea.gazarova_p_v.looper;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class MyLooper extends Thread{
    public Handler mHandler;
    private Handler mainHandler;
    public MyLooper(Handler mainThreadHandler) {
        mainHandler = mainThreadHandler;
    }

    public void run() {
        Log.d("MyLooper", "run");
        Looper.prepare();
        mHandler =  new Handler(Looper.myLooper()) {
            public void handleMessage(Message msg) {
                String data = msg.getData().getString("KEY");
                int age = msg.getData().getInt("Vosrast");
                Log.d("My Looper get message: ", data);
                Log.d("My Looper get message: ", String.valueOf(age));
                try {
                    TimeUnit.SECONDS.sleep(age);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                Message message = new Message();
                Bundle bundle = new Bundle();
                bundle.putString("result", String.format("My age is %d and my work is %s", age, data));
                message.setData(bundle);
                mainHandler.sendMessage(message);
            }
        };
        Looper.loop();
    }
}
